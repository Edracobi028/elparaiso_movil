package com.example.elparaiso_movil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterDatos extends RecyclerView.Adapter<AdapterDatos.ViewHolderDatos> {

    //Recibir una lista para mostrar
    ArrayList<String> listDatos;

    //crear un constructor de la lista de datos


    public AdapterDatos(ArrayList<String> listDatos) {
        this.listDatos = listDatos;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list,null,false);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos holder, int position) {
        holder.asignarDatos(listDatos.get(position));//enviar la info que queremos que se muestre
    }

    @Override
    public int getItemCount() {
        return listDatos.size();
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {

        //Hcer referencia al elemento
        TextView dato;

        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            //instancia o referencia con el itemView que le llega llamado idDato (item_list)
            dato = itemView.findViewById(R.id.idDato);

        }

        public void asignarDatos(String datos) {
            dato.setText(datos);//le asignemos le String que le llega
        }
    }
}
