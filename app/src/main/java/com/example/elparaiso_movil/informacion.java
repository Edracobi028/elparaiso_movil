package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class informacion extends AppCompatActivity {

    //Declarar
    public TextView tvInformacion;
    public TextView tvTel;
    public TextView tvDireccion;
    public TextView tvVersion;
    public TextView tvAño;


    //Declarar fuente
    public Typeface MontserratBlack;
    public Typeface MontserratBold;
    public Typeface MontserratLight;
    public Typeface MontserratMedium;
    public Typeface MontserratRegular;
    public Typeface MontserratSemiBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion);

        //Configurar fuente de letra
        String fuenteMontserratBlack = "fuentes/MontserratBlack.ttf";
        this.MontserratBlack = Typeface.createFromAsset(getAssets(),fuenteMontserratBlack);
        String fuenteMontserratBold = "fuentes/MontserratBold.ttf";
        this.MontserratBold = Typeface.createFromAsset(getAssets(),fuenteMontserratBold);
        String fuenteMontserratLight = "fuentes/MontserratLight.ttf";
        this.MontserratLight = Typeface.createFromAsset(getAssets(),fuenteMontserratLight);
        String fuenteMontserratMedium = "fuentes/MontserratMedium.ttf";
        this.MontserratMedium = Typeface.createFromAsset(getAssets(),fuenteMontserratMedium);
        String fuenteMontserratRegular = "fuentes/MontserratRegular.ttf";
        this.MontserratRegular = Typeface.createFromAsset(getAssets(),fuenteMontserratRegular);
        String fuenteMontserratSemiBold = "fuentes/MontserratSemiBold.ttf";
        this.MontserratSemiBold = Typeface.createFromAsset(getAssets(),fuenteMontserratSemiBold);

        //Asignar elementos

        tvTel = findViewById(R.id.tvTel);
        tvDireccion = findViewById(R.id.tvDireccion);
        tvVersion = findViewById(R.id.tvVersion);
        tvAño = findViewById(R.id.tvAño);


        //asignarle tipo letra a componentes

        tvTel.setTypeface(MontserratBold);
        tvDireccion.setTypeface(MontserratBold);
        tvVersion.setTypeface(MontserratBold);
        tvAño.setTypeface(MontserratBold);




    }
}
