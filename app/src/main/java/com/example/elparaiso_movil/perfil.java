package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class perfil extends AppCompatActivity {

    //Declarar
    public TextView tvNombre,tvInfoPersonal;
    public EditText etNombre, etApellidos, etCorreo, etTelefono,etPassword, etCurp, etRFC, etDomicilio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        //Asignar elementos
        tvNombre =findViewById(R.id.tvNombre);
        etNombre =findViewById(R.id.etNombre);
        etApellidos =findViewById(R.id.etApellidos);
        etCorreo =findViewById(R.id.etCorreo);
        etTelefono =findViewById(R.id.etTelefono);
        etPassword =findViewById(R.id.etPassword);
        etCurp =findViewById(R.id.etCurp);
        etRFC =findViewById(R.id.etRFC);
        etDomicilio =findViewById(R.id.etDomicilio);

        //Datos
        String nombre = "   Hugo";
        String apellidos = "   Torre";
        String correo = "   hugo@gmail.com";
        String telefono = "   3317852458";
        String password = "   12345678";
        String curp = "   CURPTOMH";
        String rfc = "   RFCPTOMH123";
        String domicilio = "   Tulipanes #1414 Col.Sauz";

        //Llenar campos
        tvNombre.setText(nombre + " " + apellidos);
        etNombre.setText(nombre);
        etApellidos.setText(apellidos);
        etCorreo.setText(correo);
        etTelefono.setText(telefono);
        etPassword.setText(password);
        etCurp.setText(curp);
        etRFC.setText(rfc);
        etDomicilio.setText(domicilio);



    }//onCrete
}//.Fin Clase
