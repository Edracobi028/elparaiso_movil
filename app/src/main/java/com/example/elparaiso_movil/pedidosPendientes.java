package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class pedidosPendientes extends AppCompatActivity {

    //Declarar elementos
    private TextView tv_IdPedido;
    private TextView tvEstatusPedido;
    private TextView tvFecha;
    private TextView tvTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos_pendientes);

        //Asignar elementos
        tv_IdPedido =findViewById(R.id.tv_IdPedido);
        tvEstatusPedido =findViewById(R.id.tvEstatusPedido);
        tvFecha =findViewById(R.id.tvFecha);
        tvTotal =findViewById(R.id.tvTotal);

        //Datos
        String idPedido = "12";
        String estatusPedido = "En espera";
        String fechaPedido = "27/11/2019";
        String totalPedido = "170.00";

        //Cargar datos
        tv_IdPedido.setText(idPedido);
        tvEstatusPedido.setText(estatusPedido);
        tvFecha.setText(fechaPedido);
        tvTotal.setText(totalPedido);

    }//.onCreate
}//.Fin clase
