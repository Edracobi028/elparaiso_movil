package com.example.elparaiso_movil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdaptadorArticulos extends RecyclerView.Adapter<AdaptadorArticulos.ViewHolderArticulos> {

    ArrayList<ArticuloVo> listaArticulos;

    public AdaptadorArticulos(ArrayList<ArticuloVo> listaArticulos) {
        this.listaArticulos = listaArticulos;
    }

    @NonNull
    @Override
    public ViewHolderArticulos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_articulos,null,false);
        return new ViewHolderArticulos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderArticulos holder, int position) {
        holder.etiNombre.setText(listaArticulos.get(position).getNombre());
        holder.etiInformacion.setText(listaArticulos.get(position).getInfo());
        holder.foto.setImageResource(listaArticulos.get(position).getFoto());
    }



    @Override
    public int getItemCount() {
        return listaArticulos.size();
    }

    public class ViewHolderArticulos extends RecyclerView.ViewHolder {

        //Referencia a esos componentes graficos
        TextView etiNombre,etiInformacion;
        ImageView foto;



        public ViewHolderArticulos(@NonNull View itemView) {
            super(itemView);
            etiNombre =itemView.findViewById(R.id.idNombre);
            etiInformacion =itemView.findViewById(R.id.idInfo);
            foto =itemView.findViewById(R.id.idImagen);




        }//.ViewHolderArticulos


    }//.ViewHolderArticulos




}//.fin Clase
