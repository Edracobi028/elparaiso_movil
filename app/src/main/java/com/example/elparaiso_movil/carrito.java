package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class carrito extends AppCompatActivity {

    private Button btnComprar;
    private Button btnCancelar;
    private TextView tvNombreArt,tvPrecio,tvCantidad,tvTotal;
    private TextView tvNombreArt3,tvPrecio3,tvCantidad3,tvTotal3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito);

        //Asignar elementos
        btnComprar =findViewById(R.id.btnComprar);
        btnCancelar =findViewById(R.id.btnCancelar);
        tvNombreArt =findViewById(R.id.tvNombreArt);
        tvPrecio =findViewById(R.id.tvPrecio);
        tvCantidad =findViewById(R.id.tvCantidad);
        tvTotal =findViewById(R.id.tvTotal);

        tvNombreArt3 =findViewById(R.id.tvNombreArt3);
        tvPrecio3 =findViewById(R.id.tvPrecio3);
        tvCantidad3 =findViewById(R.id.tvCantidad3);
        tvTotal3 =findViewById(R.id.tvTotal3);


        //Llenado datos
        String nombreArt = "Disco Nylon Flying Disc";
        String  precioArt = "145.00";
        String cantArt = "1";
        String totalArt = "145.00";

        String nombreArt3 = "Cascabel de metal";
        String  precioArt3 = "25.00";
        String cantArt3 = "1";
        String totalArt3 = "25.00";


        //Llenado datos
        tvNombreArt.setText(nombreArt);
        tvPrecio.setText(precioArt);
        tvCantidad.setText(cantArt);
        tvTotal.setText(totalArt);

        tvNombreArt3.setText(nombreArt3);
        tvPrecio3.setText(precioArt3);
        tvCantidad3.setText(cantArt3);
        tvTotal3.setText(totalArt3);


        btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(carrito.this, seleccionarPago.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(carrito.this, catalogo.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener

    }//.onCreate
}//.Fin Clase
