package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;


import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.util.*;
import android.widget.Toast;

import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class login extends AppCompatActivity {
    //Declarar
    private Button jbtnIngresar_login;

    private EditText jtxtUsuariologin;
    private EditText jtxtPasswordlogin;

    private static String URL_LOGIN ="http://localhost/servicios_php/validar_usuario.php";


    private Button jbtnRegistrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        jbtnIngresar_login = findViewById(R.id.btnIngresar_login);
        jtxtUsuariologin = findViewById(R.id.txtUsuariologin);
        jtxtPasswordlogin = findViewById(R.id.txtPasswordlogin);

        jbtnRegistrar = findViewById(R.id.btnRegistrar);


        jbtnIngresar_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = jtxtUsuariologin.getText().toString().trim();
                String password = jtxtPasswordlogin.getText().toString().trim();
                if(!usuario.isEmpty() || !password.isEmpty()){

                   login(usuario, password);



                    //login(usuario, password);
                    Intent intent = new Intent(login.this, MainActivity.class);
                    startActivity(intent);

                }else{
                    jtxtUsuariologin.setError("Porfavor Ingrese su Correo");
                    jtxtPasswordlogin.setError("Porfavor Ingrese su Contraseña");
                }

            }
        });





        jbtnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(login.this, Registro.class);
                startActivity(intent);

            }
        });

    }


        private void login(String usuario, String password){

        //jbtnIngresar_login.setVisibility(View.GONE);

            StringRequest stringRequest=new StringRequest(Request.Method.POST, URL_LOGIN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String success = jsonObject.getString("success");
                        JSONArray jsonArray = jsonObject.getJSONArray("login");

                        if(success.equals("1")){

                            for(int i = 0; i < jsonArray.length(); i++){


                                JSONObject object = jsonArray.getJSONObject(i);

                                String nombre = object.getString("nombre").trim();
                                String email = object.getString("email").trim();


                                Intent intent = new Intent(login.this, home.class);
                                intent.putExtra("nombre", nombre);
                                intent.putExtra("email", email);
                                startActivity(intent);

                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        jbtnIngresar_login.setVisibility(View.GONE);
                        Toast.makeText(login.this, "Error "+e.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    jbtnIngresar_login.setVisibility(View.VISIBLE);
                   // Toast.makeText(login.this, "", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> parametros = new HashMap<String,String>();
                    parametros.put("email", jtxtUsuariologin.getText().toString());
                    parametros.put("password", jtxtPasswordlogin.getText().toString());
                    return parametros;
                }
            };

            RequestQueue requestQueue= Volley.newRequestQueue(this);

            requestQueue.add(stringRequest);

        }



    }
