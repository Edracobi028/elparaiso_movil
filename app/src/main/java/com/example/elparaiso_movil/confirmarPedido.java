package com.example.elparaiso_movil;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class confirmarPedido extends AppCompatActivity {

    //Declarar
    public Button btnConfirmar;
    public Button btnCancelar;

    //Declarar fuente
    public Typeface MontserratSemiBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar_pedido);

        //Asignar elementos
        btnConfirmar =findViewById(R.id.btnConfirmar);
        btnCancelar =findViewById(R.id.btnCancelar);

        //asignarle tipo letra a componentes
        btnConfirmar.setTypeface(MontserratSemiBold);
        btnCancelar.setTypeface(MontserratSemiBold);

        //Configurar fuente de letra
        String fuenteMontserratSemiBold = "fuentes/MontserratSemiBold.ttf";
        this.MontserratSemiBold = Typeface.createFromAsset(getAssets(),fuenteMontserratSemiBold);

        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(confirmarPedido.this, splash_exito.class);
                startActivity(intent);
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(confirmarPedido.this, carrito.class);
                startActivity(intent);
            }
        });


    }//.onCreate



}//.Fin Clase
