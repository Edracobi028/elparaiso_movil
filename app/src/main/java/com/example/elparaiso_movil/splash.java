package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;




public class splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //tiempo splash
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //indicar lo proximo a abrir
                Intent intent= new Intent(splash.this, login.class);
                startActivity(intent);
                finish();
            }
        },4000);

    }//.oncreate
}//.Clase splash
