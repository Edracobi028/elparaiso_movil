package com.example.elparaiso_movil;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    //Declarar
    public Button btnCatalogo;
    public Button btnCitas;
    public Button btnPedidos;
    //public Button btnPerfil;
    //public Button btnCerrarSesion;
    //public Button btnInformacion;
    //public Button btnNotificaciones;

    //Declarar fuente
    public Typeface MontserratBlack;
    public Typeface MontserratBold;
    public Typeface MontserratLight;
    public Typeface MontserratMedium;
    public Typeface MontserratRegular;
    public Typeface MontserratSemiBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Configurar fuente de letra
        String fuenteMontserratBlack = "fuentes/MontserratBlack.ttf";
        this.MontserratBlack = Typeface.createFromAsset(getAssets(),fuenteMontserratBlack);
        String fuenteMontserratBold = "fuentes/MontserratBold.ttf";
        this.MontserratBold = Typeface.createFromAsset(getAssets(),fuenteMontserratBold);
        String fuenteMontserratLight = "fuentes/MontserratLight.ttf";
        this.MontserratLight = Typeface.createFromAsset(getAssets(),fuenteMontserratLight);
        String fuenteMontserratMedium = "fuentes/MontserratMedium.ttf";
        this.MontserratMedium = Typeface.createFromAsset(getAssets(),fuenteMontserratMedium);
        String fuenteMontserratRegular = "fuentes/MontserratRegular.ttf";
        this.MontserratRegular = Typeface.createFromAsset(getAssets(),fuenteMontserratRegular);
        String fuenteMontserratSemiBold = "fuentes/MontserratSemiBold.ttf";
        this.MontserratSemiBold = Typeface.createFromAsset(getAssets(),fuenteMontserratSemiBold);

        //Asignar elementos
        btnCatalogo =findViewById(R.id.btnCatalogo);
        btnCitas =findViewById(R.id.btnCitas);
        btnPedidos =findViewById(R.id.btnPedidos);
        //btnPerfil =findViewById(R.id.btnPerfil);
        //btnCerrarSesion =findViewById(R.id.btnCerrarSesion);
        //btnInformacion =findViewById(R.id.btnInformacion);
        //btnNotificaciones=findViewById(R.id.btnNotificaciones);

        //asignarle tipo letra a componentes
        btnCatalogo.setTypeface(MontserratBold);
        btnCitas.setTypeface(MontserratBold);
        btnPedidos.setTypeface(MontserratBold);
        //btnPerfil.setTypeface(MontserratBold);
        //btnCerrarSesion.setTypeface(MontserratSemiBold);
        //btnInformacion.setTypeface(MontserratSemiBold);
        //btnNotificaciones.setTypeface(MontserratSemiBold);

        btnCatalogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, catalogo.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener

        btnCitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, citas.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener


        btnPedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, pedidos.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener
/*
        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, perfil.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener

        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, login.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener

        btnInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, informacion.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener

        btnNotificaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, notificaciones.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener
*/
    }//.onCreate

    //Toolbar:
    @Override public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_principal,menu);
        return  true;
    }//.onCreateOptionsMenu

    //Toolbar: Cargar
    @Override public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.notificaciones:
                Intent intent = new Intent(MainActivity.this, notificaciones.class);
                startActivity(intent);
                return true;
            case R.id.perfil:
                Intent intent3 = new Intent(MainActivity.this, perfil.class);
                startActivity(intent3);
                return true;
            case R.id.cerrarSesion:
                Intent intent4 = new Intent(MainActivity.this, login.class);
                startActivity(intent4);
                return true;
            case R.id.informacion:
                Intent intent5 = new Intent(MainActivity.this, informacion.class);
                startActivity(intent5);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }//.switch
    }//.onOptionItemSelected

}//fin de clase
