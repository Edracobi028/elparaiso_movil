package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class pedidoCompletados extends AppCompatActivity {

    //Declarar elementos
    private TextView tv_IdPedido;
    private TextView tvEstatusPedido;
    private TextView tvFecha;
    private TextView tvTotal;

    private TextView tv_IdPedido2;
    private TextView tvEstatusPedido2;
    private TextView tvFecha2;
    private TextView tvTotal2;

    private TextView tv_IdPedido3;
    private TextView tvEstatusPedido3;
    private TextView tvFecha3;
    private TextView tvTotal4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido_completados);

        //Asignar elementos
        tv_IdPedido =findViewById(R.id.tv_IdPedido);
        tvEstatusPedido =findViewById(R.id.tvEstatusPedido);
        tvFecha =findViewById(R.id.tvFecha);
        tvTotal =findViewById(R.id.tvTotal);

        tv_IdPedido2 =findViewById(R.id.tv_IdPedido2);
        tvEstatusPedido2 =findViewById(R.id.tvEstatusPedido2);
        tvFecha2 =findViewById(R.id.tvFecha2);
        tvTotal2 =findViewById(R.id.tvTotal2);

        tv_IdPedido3 =findViewById(R.id.tv_IdPedido3);
        tvEstatusPedido3 =findViewById(R.id.tvEstatusPedido3);
        tvFecha3 =findViewById(R.id.tvFecha3);
        tvTotal4 =findViewById(R.id.tvTotal4);

        //Datos
        String idPedido = "11";
        String estatusPedido = "Entregado";
        String fechaPedido = "27/11/2019";
        String totalPedido = "811.00";

        String idPedido2 = "6";
        String estatusPedido2 = "Entregado";
        String fechaPedido2 = "26/11/2019";
        String totalPedido2 = "65.00";

        String idPedido3 = "2";
        String estatusPedido3 = "Entregado";
        String fechaPedido3 = "24/11/2019";
        String totalPedido3 = "135.00";

        //Cargar datos
        tv_IdPedido.setText(idPedido);
        tvEstatusPedido.setText(estatusPedido);
        tvFecha.setText(fechaPedido);
        tvTotal.setText(totalPedido);

        tv_IdPedido2.setText(idPedido2);
        tvEstatusPedido2.setText(estatusPedido2);
        tvFecha2.setText(fechaPedido2);
        tvTotal2.setText(totalPedido2);

        tv_IdPedido3.setText(idPedido3);
        tvEstatusPedido3.setText(estatusPedido3);
        tvFecha3.setText(fechaPedido3);
        tvTotal4.setText(totalPedido3);

    }
}
