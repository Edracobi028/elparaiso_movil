package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class splash_exito extends AppCompatActivity {

    //Declarar
    public TextView tvExito1;
    public TextView tvExito2;

    //Declarar fuente
    public Typeface MontserratRegular;
    public Typeface MontserratBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_exito);

        //Configurar fuente de letra
        String fuenteMontserratRegular = "fuentes/MontserratRegular.ttf";
        this.MontserratRegular = Typeface.createFromAsset(getAssets(),fuenteMontserratRegular);
        String fuenteMontserratBold = "fuentes/MontserratBold.ttf";
        this.MontserratBold = Typeface.createFromAsset(getAssets(),fuenteMontserratBold);

        //Asignar elementos
        tvExito1 =findViewById(R.id.tvExito1);
        tvExito2 =findViewById(R.id.tvExito2);


        //ASIGNAR tipo letra a componentes
        tvExito1.setTypeface(MontserratRegular);
        tvExito2.setTypeface(MontserratBold);

        //tiempo splash
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //indicar lo proximo a abrir
                Intent intent= new Intent(splash_exito.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        },2000);
    }
}
