package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.net.URL;

public class listadoCitas extends AppCompatActivity {

    //Declarar el list view
    ListView lista;

    //Frank: RequestQueue requestQueue;


    //Matriz para almacenar datos
    String[][] datos = {
            {"Pechin","28/11/19", "07:00 pm - 08:00 pm"},
            {"Luke","39/12/19", "04:00 pm - 05:00 pm"},
            {"Pechin","28/12/20", "04:00 pm - 05:00 pm"},

    };

    //Frank: Array citas;

    //Frank: String[][] datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Frank: String URL = "http://192.168.1.103/servicios_php/obtenerCitas.php";

        //Frank:   this.listarCitas(URL);

        setContentView(R.layout.activity_listado_citas);
        //instanciar
        lista =(ListView) findViewById(R.id.lvListaCitas);

        //Instanciando un nuevo adaptador y asigandoselo a listView
        lista.setAdapter(new Adaptador(this, datos ));

    }//.onCreate

    //Frank:
     /*
    private void listarCitas(String URL){

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URL, new Response.Listener<JSONArray>(){
            @Override
            public void onResponse(JSONArray response){
                JSONObject jsonObject = null;

                for (int i = 0; i < response.length(); i++){
                    //Resultado
                    try {
                        jsonObject = response.getJSONObject(i);

                        Toast.makeText(getApplicationContext(), "FELICIDADES", Toast.LENGTH_SHORT).show();
                    }catch(JSONException e){
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        );

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);

    }
    */

}//.Clase listadoCitas
