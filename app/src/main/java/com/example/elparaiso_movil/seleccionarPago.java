package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class seleccionarPago extends AppCompatActivity {

    //Declarar
    public  Button btnPagoTienda;
    public Button btnPagoPaypal;

    //Declarar fuente
    public Typeface MontserratSemiBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_pago);

        //Asignar elementos
        btnPagoTienda =findViewById(R.id.btnPagoTienda);
        btnPagoPaypal =findViewById(R.id.btnPagoPaypal);

        //Configurar fuente de letra
        String fuenteMontserratSemiBold = "fuentes/MontserratSemiBold.ttf";
        this.MontserratSemiBold = Typeface.createFromAsset(getAssets(),fuenteMontserratSemiBold);

        //asignarle tipo letra a componentes
        btnPagoTienda.setTypeface(MontserratSemiBold);

        btnPagoTienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(seleccionarPago.this, confirmarPedido.class);
                startActivity(intent);
            }
        });

        btnPagoPaypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(seleccionarPago.this, confirmarPedidoPayPal.class);
                startActivity(intent);
            }
        });


        //Declarar fuente
    }
}
