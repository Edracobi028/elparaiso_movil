package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class pedidos extends AppCompatActivity {

    //Declarar elementos
    public Button btnEstatusPedido;
    public Button btnConsultarPedido;

    //Declarar fuente
    public Typeface MontserratBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);

        //Configurar fuente de letra
        String fuenteMontserratBold = "fuentes/MontserratBold.ttf";
        this.MontserratBold = Typeface.createFromAsset(getAssets(),fuenteMontserratBold);

        //Asignar elementos
        btnEstatusPedido =findViewById(R.id.btnEstatusPedido);
        btnConsultarPedido =findViewById(R.id.btnConsultarPedido);

        //asignarle tipo letra a componentes
        btnEstatusPedido.setTypeface(MontserratBold);
        btnConsultarPedido.setTypeface(MontserratBold);

        btnEstatusPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(pedidos.this, pedidosPendientes.class);
                startActivity(intent);
            }//.onClick
        });//.setOnClickListener

        btnConsultarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(pedidos.this, pedidoCompletados.class);
                startActivity(intent);
            }//.onClick
        });//.setOnClickListener

    }//onCreate
}//.Fin clase
