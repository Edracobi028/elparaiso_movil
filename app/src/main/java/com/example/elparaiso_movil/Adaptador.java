package com.example.elparaiso_movil;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Adaptador extends BaseAdapter {

    private static LayoutInflater inflater = null;

    Context contexto;
    String[][] datos;

    //Constructor
    public Adaptador(Context contexto, String[][] datos){
        this.contexto = contexto;
        this.datos = datos;
        inflater = (LayoutInflater)contexto.getSystemService(contexto.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        final View vista = inflater.inflate(R.layout.elemento_lista,null);
        TextView titulo = (TextView)vista.findViewById(R.id.tvTitulo);
        TextView duracion = (TextView)vista.findViewById(R.id.tvDuracion);
        TextView director = (TextView)vista.findViewById(R.id.tvDirector);
        titulo.setText(datos[i][0]);
        director.setText(datos[i][1]);
        duracion.setText("Hora cita: "+datos[i][2]);

        return vista;
    }

    @Override
    public int getCount() {
        return datos.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


}
