package com.example.elparaiso_movil;

public class ArticuloVo {
    //Poner los atributos de nuestro objeto
    private String nombre;
    private String info;
    private int foto;

    public ArticuloVo(){}

    //Constructor
    public ArticuloVo(String nombre, String info, int foto) {
        this.nombre = nombre;
        this.info = info;
        this.foto = foto;
    }//.Constructor

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }
}//.Fin Clase
