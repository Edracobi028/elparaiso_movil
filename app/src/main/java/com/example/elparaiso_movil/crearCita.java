package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;



import java.util.ArrayList;
import java.util.Calendar;

public class crearCita extends AppCompatActivity {

    //Declarar elementos
    public Spinner spinnerMascota;
    public Spinner spinnerHora;
    public TextView tvSelecMascota;
    public TextView tvSelecDia;
    public TextView tvSelecHora;
    public EditText etFecha;
    public Button btnFecha;
    public Button btnAceptar;
    public Button btnCancelar;
    public int dia,mes,ano;


    //Declarar fuente
    public Typeface MontserratBlack;
    public Typeface MontserratBold;
    public Typeface MontserratSemiBold;
    public Typeface MontserratMedium;
    public Typeface MontserratRegular;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cita);

        //Configurar fuente de letra
        String fuenteMontserratBlack = "fuentes/MontserratBlack.ttf";
        this.MontserratBlack = Typeface.createFromAsset(getAssets(),fuenteMontserratBlack);
        String fuenteMontserratBold = "fuentes/MontserratBold.ttf";
        this.MontserratBold = Typeface.createFromAsset(getAssets(),fuenteMontserratBold);
        String fuenteMontserratMedium = "fuentes/MontserratMedium.ttf";
        this.MontserratMedium = Typeface.createFromAsset(getAssets(),fuenteMontserratMedium);
        String fuenteMontserratRegular = "fuentes/MontserratRegular.ttf";
        this.MontserratRegular = Typeface.createFromAsset(getAssets(),fuenteMontserratRegular);
        String fuenteMontserratSemiBold = "fuentes/MontserratSemiBold.ttf";
        this.MontserratSemiBold = Typeface.createFromAsset(getAssets(),fuenteMontserratSemiBold);

        //Asignar elementos
        spinnerMascota =findViewById(R.id.spinnerMascota);
        spinnerHora =findViewById(R.id.spinnerHora);
        tvSelecMascota =findViewById(R.id.tvSelecMascota);
        tvSelecDia = findViewById(R.id.tvSelecDia);
        tvSelecHora = findViewById(R.id.tvSelecHora);
        etFecha = findViewById(R.id.etFecha);
        btnFecha = findViewById(R.id.btnFecha);
        btnAceptar = findViewById(R.id.btnAceptar);
        btnCancelar = findViewById(R.id.btnCancelar);


        //ASIGNAR tipo letra a componentes
        tvSelecMascota.setTypeface(MontserratSemiBold);
        tvSelecDia.setTypeface(MontserratSemiBold);
        tvSelecHora.setTypeface(MontserratSemiBold);
        btnAceptar.setTypeface(MontserratSemiBold);
        btnCancelar.setTypeface(MontserratSemiBold);

        // == SPINNER MASCOTA ==
        //Lista para cargara Spinner
        ArrayList<String> elementos = new ArrayList<>();
        elementos.add("");
        elementos.add("Pechin");
        elementos.add("Puppy");

        //El adaptador
        ArrayAdapter adaptador = new ArrayAdapter(crearCita.this, android.R.layout.simple_spinner_dropdown_item, elementos);

        //asignar el adaptador al spinner
        spinnerMascota.setAdapter(adaptador);

        //Agregarle los listeners cuando presionamos uno de ellos
        spinnerMascota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //identificar el elemento seleccionado
                String elemento= (String) spinnerMascota.getAdapter().getItem(position);

                //Toast.makeText(crearCita.this, "Seleccionaste: "+elemento, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // == CALENDARIO  ==
        btnFecha.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v==btnFecha){
                    final Calendar c= Calendar.getInstance();
                    dia=c.get(Calendar.DAY_OF_MONTH);
                    mes=c.get(Calendar.MONTH);
                    ano=c.get(Calendar.YEAR);
                    DatePickerDialog datePickerDialog = new DatePickerDialog(crearCita.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            etFecha.setText(dayOfMonth+"/"+month+"/"+year);

                        }

                    }
                    ,dia,mes,ano);
                    datePickerDialog.show();
                }//.if

            }//.ononClick
        });


        // == SPINNER HORAS ==

        //Lista para cargara Spinner
        ArrayList<String> horas = new ArrayList<>();
        horas.add("");
        horas.add("10:00 am - 11:00 am");
        horas.add("01:00 pm - 02:00 pm");
        horas.add("04:00 pm - 05:00 pm");
        horas.add("07:00 pm - 08:00 pm");

        //El adaptador
        ArrayAdapter adpHoras = new ArrayAdapter(crearCita.this, android.R.layout.simple_spinner_dropdown_item, horas);

        //asignar el adaptador al spinner
        spinnerHora.setAdapter(adpHoras);

        //Agregarle los listeners cuando presionamos uno de ellos
        spinnerHora.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //identificar el elemento seleccionado
                String horas= (String) spinnerHora.getAdapter().getItem(position);

                //Toast.makeText(crearCita.this, "Seleccionaste: "+elemento, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // == BOTON CANCELAR ==

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(crearCita.this, citas.class);
                startActivity(intent);
            }//.onClick
        });//.setOnClickListener


        // == BOTON ACEPTAR ==
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(crearCita.this, splash_exito.class);
                startActivity(intent);
            }//.onClick
        });//.setOnClickListener

    }//.onCreate
}//.Clase crearCita
