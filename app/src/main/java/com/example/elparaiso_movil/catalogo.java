package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class catalogo extends AppCompatActivity {

    //RecyclerView: Declarar
    ArrayList<ArticuloVo> listaArticulos;
    RecyclerView recyclerArticulos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogo);

        //RecyclerView:
        listaArticulos=new ArrayList<>();
        recyclerArticulos = findViewById(R.id.recyclerId);
        recyclerArticulos.setLayoutManager(new LinearLayoutManager(this));

        //RecyclerView: Se encarga de llenar nuestra lista
        llenarArticulos();

        //RecyclerView:mandar lista la adaptador
        AdaptadorArticulos adapter = new AdaptadorArticulos(listaArticulos);
        recyclerArticulos.setAdapter(adapter);
    }//onCreate


    //Toolbar:
    @Override public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_catalogo,menu);
        return  true;
    }//.onCreateOptionsMenu

    //Toolbar: Cargar
    @Override public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.notificaciones:
                Intent intent = new Intent(catalogo.this, notificaciones.class);
                startActivity(intent);
                return true;
            case R.id.perfil:
                Intent intent3 = new Intent(catalogo.this, perfil.class);
                startActivity(intent3);
                return true;
            case R.id.cerrarSesion:
                Intent intent4 = new Intent(catalogo.this, login.class);
                startActivity(intent4);
                return true;
            case R.id.informacion:
                Intent intent5 = new Intent(catalogo.this, informacion.class);
                startActivity(intent5);
                return true;
            case R.id.carrito:
                Intent intent6 = new Intent(catalogo.this, carrito.class);
                startActivity(intent6);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }//.switch


    }//.onOptionItemSelected


    //Recycler:
    private void llenarArticulos(){
        listaArticulos.add(new ArticuloVo("Tapete Entrenador Green Carpet", "$ 629.0", R.mipmap.tapede_mediano));
        listaArticulos.add(new ArticuloVo("Collar de Microfibra - Chico", "$ 135.00", R.mipmap.collar_chico2));
        listaArticulos.add(new ArticuloVo("Disco Nylon Flying Disc", "$ 145.00", R.mipmap.disco_nerf_azul));
        listaArticulos.add(new ArticuloVo("Arnés Unimax - Mediano", "$ 666.00", R.mipmap.arnes_mediana));
        listaArticulos.add(new ArticuloVo("Correa de Microfibra - Chico", "$ 175.00", R.mipmap.correachica2));
        listaArticulos.add(new ArticuloVo("Mini Bones de Cerdo para Perro", "$ 90.00", R.mipmap.mini_bone));
        listaArticulos.add(new ArticuloVo("Porky Pop de Cerdo para Perros", "$ 75.00", R.mipmap.porky_pop));
        listaArticulos.add(new ArticuloVo("Puppy", "$ 115.00", R.mipmap.diamond_puppy));
        listaArticulos.add(new ArticuloVo("Arena Scoop Free", "$ 262.00", R.mipmap.arenero));
        listaArticulos.add(new ArticuloVo("Plato Anti-Hormiga Cats in Love 210 ml", "$ 154.00", R.mipmap.plato_gato));
        listaArticulos.add(new ArticuloVo("Minino Plus 1.3 Kg", "$ 65.00", R.mipmap.minino1_3kg));
        listaArticulos.add(new ArticuloVo("Ratón de Fieltro con Movimiento", "$ 82.00", R.mipmap.raton_fieltro));
        listaArticulos.add(new ArticuloVo("Set de Pelotas de Juego", "$ 65.00", R.mipmap.bola_azul));
        listaArticulos.add(new ArticuloVo("Arena para Gatos Cat's Best", "$ 139.00", R.mipmap.arena_gatos));
        listaArticulos.add(new ArticuloVo("Cascabel de metal", "$ 25.00", R.mipmap.cscabel_metal));
        listaArticulos.add(new ArticuloVo("Cat Grass de Trigo", "$ 86.00", R.mipmap.pasto_trigo));


    }
}//.fin clase
