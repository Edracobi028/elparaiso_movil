package com.example.elparaiso_movil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class citas extends AppCompatActivity {

    //Declarar elementos
    public Button btnCrearCita;
    public Button btnConsultarCitas;


    //Declarar fuente
    public Typeface MontserratBlack;
    public Typeface MontserratBold;
    public Typeface MontserratMedium;
    public Typeface MontserratRegular;
    public Typeface MontserratSemiBold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citas);

        //Configurar fuente de letra
        String fuenteMontserratBlack = "fuentes/MontserratBlack.ttf";
        this.MontserratBlack = Typeface.createFromAsset(getAssets(),fuenteMontserratBlack);
        String fuenteMontserratBold = "fuentes/MontserratBold.ttf";
        this.MontserratBold = Typeface.createFromAsset(getAssets(),fuenteMontserratBold);
        String fuenteMontserratMedium = "fuentes/MontserratMedium.ttf";
        this.MontserratMedium = Typeface.createFromAsset(getAssets(),fuenteMontserratMedium);
        String fuenteMontserratRegular = "fuentes/MontserratRegular.ttf";
        this.MontserratRegular = Typeface.createFromAsset(getAssets(),fuenteMontserratRegular);
        String fuenteMontserratSemiBold = "fuentes/MontserratSemiBold.ttf";
        this.MontserratSemiBold = Typeface.createFromAsset(getAssets(),fuenteMontserratSemiBold);

        //Asignar elementos
        btnCrearCita =findViewById(R.id.btnCrearCita);
        btnConsultarCitas =findViewById(R.id.btnConsultarCitas);

        //asignarle tipo letra a componentes
        btnCrearCita.setTypeface(MontserratBold);
        btnConsultarCitas.setTypeface(MontserratBold);

        btnCrearCita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(citas.this, crearCita.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener

        btnConsultarCitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(citas.this, listadoCitas.class);
                startActivity(intent);
            }//.onClick
        });//setOnClickListener

    }//onCreate
}//Clase citas
